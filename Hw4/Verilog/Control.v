module Control
(
    Op_i,
    RegDst_o,
    ALUOp_o,
    ALUSrc_o,
    RegWrite_o
);

input	[5:0]	Op_i;
output			RegDst_o;
output	[1:0]	ALUOp_o;
output			ALUSrc_o;
output			RegWrite_o;

reg				RegDst_o;
reg		[1:0]	ALUOp_o;
reg				ALUSrc_o;
reg				RegWrite_o;

always@(Op_i) begin
	if (Op_i == 5'b00000) begin
		RegDst_o	= 1'b1;
		ALUOp_o		= 2'b10;
		ALUSrc_o	= 1'b0;
		RegWrite_o	= 1'b1;
	end
	else begin
		RegDst_o	= 1'b0;
		ALUOp_o		= 2'b00;
		ALUSrc_o	= 1'b1;
		RegWrite_o	= 1'b1;
	end
end

endmodule
