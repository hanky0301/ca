#include <cstdlib>
#include <iostream>
using namespace std;

int func(int n, int c)
{
	if (n == 1)
		return c;
	return 2 * func(n / 2, c) + c * n;
}

int main(int argc, char* argv[])
{
	int n = atoi(argv[1]);
	int c = atoi(argv[2]);
	cout << func(n, c) << endl;

	return 0;
}
